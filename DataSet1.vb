﻿'------------------------------------------------------------------------------
' <autogenerated>
'     This code was generated by a tool.
'     Runtime Version: 1.1.4322.2032
'
'     Changes to this file may cause incorrect behavior and will be lost if 
'     the code is regenerated.
' </autogenerated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.Data
Imports System.Runtime.Serialization
Imports System.Xml


<Serializable(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Diagnostics.DebuggerStepThrough(),  _
 System.ComponentModel.ToolboxItem(true)>  _
Public Class DataSet1
    Inherits DataSet
    
    Private tablekjendis As kjendisDataTable
    
    Public Sub New()
        MyBase.New
        Me.InitClass
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New
        Dim strSchema As String = CType(info.GetValue("XmlSchema", GetType(System.String)),String)
        If (Not (strSchema) Is Nothing) Then
            Dim ds As DataSet = New DataSet
            ds.ReadXmlSchema(New XmlTextReader(New System.IO.StringReader(strSchema)))
            If (Not (ds.Tables("kjendis")) Is Nothing) Then
                Me.Tables.Add(New kjendisDataTable(ds.Tables("kjendis")))
            End If
            Me.DataSetName = ds.DataSetName
            Me.Prefix = ds.Prefix
            Me.Namespace = ds.Namespace
            Me.Locale = ds.Locale
            Me.CaseSensitive = ds.CaseSensitive
            Me.EnforceConstraints = ds.EnforceConstraints
            Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
            Me.InitVars
        Else
            Me.InitClass
        End If
        Me.GetSerializationData(info, context)
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    <System.ComponentModel.Browsable(false),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)>  _
    Public ReadOnly Property kjendis As kjendisDataTable
        Get
            Return Me.tablekjendis
        End Get
    End Property
    
    Public Overrides Function Clone() As DataSet
        Dim cln As DataSet1 = CType(MyBase.Clone,DataSet1)
        cln.InitVars
        Return cln
    End Function
    
    Protected Overrides Function ShouldSerializeTables() As Boolean
        Return false
    End Function
    
    Protected Overrides Function ShouldSerializeRelations() As Boolean
        Return false
    End Function
    
    Protected Overrides Sub ReadXmlSerializable(ByVal reader As XmlReader)
        Me.Reset
        Dim ds As DataSet = New DataSet
        ds.ReadXml(reader)
        If (Not (ds.Tables("kjendis")) Is Nothing) Then
            Me.Tables.Add(New kjendisDataTable(ds.Tables("kjendis")))
        End If
        Me.DataSetName = ds.DataSetName
        Me.Prefix = ds.Prefix
        Me.Namespace = ds.Namespace
        Me.Locale = ds.Locale
        Me.CaseSensitive = ds.CaseSensitive
        Me.EnforceConstraints = ds.EnforceConstraints
        Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
        Me.InitVars
    End Sub
    
    Protected Overrides Function GetSchemaSerializable() As System.Xml.Schema.XmlSchema
        Dim stream As System.IO.MemoryStream = New System.IO.MemoryStream
        Me.WriteXmlSchema(New XmlTextWriter(stream, Nothing))
        stream.Position = 0
        Return System.Xml.Schema.XmlSchema.Read(New XmlTextReader(stream), Nothing)
    End Function
    
    Friend Sub InitVars()
        Me.tablekjendis = CType(Me.Tables("kjendis"),kjendisDataTable)
        If (Not (Me.tablekjendis) Is Nothing) Then
            Me.tablekjendis.InitVars
        End If
    End Sub
    
    Private Sub InitClass()
        Me.DataSetName = "DataSet1"
        Me.Prefix = ""
        Me.Namespace = "http://www.tempuri.org/DataSet1.xsd"
        Me.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.CaseSensitive = false
        Me.EnforceConstraints = true
        Me.tablekjendis = New kjendisDataTable
        Me.Tables.Add(Me.tablekjendis)
    End Sub
    
    Private Function ShouldSerializekjendis() As Boolean
        Return false
    End Function
    
    Private Sub SchemaChanged(ByVal sender As Object, ByVal e As System.ComponentModel.CollectionChangeEventArgs)
        If (e.Action = System.ComponentModel.CollectionChangeAction.Remove) Then
            Me.InitVars
        End If
    End Sub
    
    Public Delegate Sub kjendisRowChangeEventHandler(ByVal sender As Object, ByVal e As kjendisRowChangeEvent)
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class kjendisDataTable
        Inherits DataTable
        Implements System.Collections.IEnumerable
        
        Private columnkjendisID As DataColumn
        
        Private columnfornavn As DataColumn
        
        Private columnetternavn As DataColumn
        
        Private columnadresse As DataColumn
        
        Private columntlf As DataColumn
        
        Private columnfødselsdato As DataColumn

        Private columnpostnr As DataColumn

        Private columnpost_sted As DataColumn

        Private columndødsdato As DataColumn

        Private columnemail As DataColumn

        Private columnwww As DataColumn

        Private columninfo As DataColumn

        Private columndød As DataColumn

        Private columnomtale As DataColumn

        Friend Sub New()
            MyBase.New("kjendis")
            Me.InitClass()
        End Sub

        Friend Sub New(ByVal table As DataTable)
            MyBase.New(table.TableName)
            If (table.CaseSensitive <> table.DataSet.CaseSensitive) Then
                Me.CaseSensitive = table.CaseSensitive
            End If
            If (table.Locale.ToString <> table.DataSet.Locale.ToString) Then
                Me.Locale = table.Locale
            End If
            If (table.Namespace <> table.DataSet.Namespace) Then
                Me.Namespace = table.Namespace
            End If
            Me.Prefix = table.Prefix
            Me.MinimumCapacity = table.MinimumCapacity
            Me.DisplayExpression = table.DisplayExpression
        End Sub

        <System.ComponentModel.Browsable(False)> _
        Public ReadOnly Property Count() As Integer
            Get
                Return Me.Rows.Count
            End Get
        End Property

        Friend ReadOnly Property kjendisIDColumn() As DataColumn
            Get
                Return Me.columnkjendisID
            End Get
        End Property

        Friend ReadOnly Property fornavnColumn() As DataColumn
            Get
                Return Me.columnfornavn
            End Get
        End Property

        Friend ReadOnly Property etternavnColumn() As DataColumn
            Get
                Return Me.columnetternavn
            End Get
        End Property

        Friend ReadOnly Property adresseColumn() As DataColumn
            Get
                Return Me.columnadresse
            End Get
        End Property

        Friend ReadOnly Property tlfColumn() As DataColumn
            Get
                Return Me.columntlf
            End Get
        End Property

        Friend ReadOnly Property fødselsdatoColumn() As DataColumn
            Get
                Return Me.columnfødselsdato
            End Get
        End Property

        Friend ReadOnly Property postnrColumn() As DataColumn
            Get
                Return Me.columnpostnr
            End Get
        End Property

        Friend ReadOnly Property post_stedColumn() As DataColumn
            Get
                Return Me.columnpost_sted
            End Get
        End Property

        Friend ReadOnly Property dødsdatoColumn() As DataColumn
            Get
                Return Me.columndødsdato
            End Get
        End Property

        Friend ReadOnly Property emailColumn() As DataColumn
            Get
                Return Me.columnemail
            End Get
        End Property

        Friend ReadOnly Property wwwColumn() As DataColumn
            Get
                Return Me.columnwww
            End Get
        End Property

        Friend ReadOnly Property infoColumn() As DataColumn
            Get
                Return Me.columninfo
            End Get
        End Property

        Friend ReadOnly Property dødColumn() As DataColumn
            Get
                Return Me.columndød
            End Get
        End Property

        Friend ReadOnly Property omtaleColumn() As DataColumn
            Get
                Return Me.columnomtale
            End Get
        End Property

        Default Public ReadOnly Property Item(ByVal index As Integer) As kjendisRow
            Get
                Return CType(Me.Rows(index), kjendisRow)
            End Get
        End Property

        Public Event kjendisRowChanged As kjendisRowChangeEventHandler

        Public Event kjendisRowChanging As kjendisRowChangeEventHandler

        Public Event kjendisRowDeleted As kjendisRowChangeEventHandler

        Public Event kjendisRowDeleting As kjendisRowChangeEventHandler

        Public Overloads Sub AddkjendisRow(ByVal row As kjendisRow)
            Me.Rows.Add(row)
        End Sub

        Public Overloads Function AddkjendisRow(ByVal fornavn As String, ByVal etternavn As String, ByVal adresse As String, ByVal tlf As String, ByVal fødselsdato As Date, ByVal postnr As String, ByVal post_sted As String, ByVal dødsdato As Date, ByVal email As String, ByVal www As String, ByVal info As String, ByVal død As Boolean, ByVal omtale As Boolean) As kjendisRow
            Dim rowkjendisRow As kjendisRow = CType(Me.NewRow, kjendisRow)
            rowkjendisRow.ItemArray = New Object() {Nothing, fornavn, etternavn, adresse, tlf, fødselsdato, postnr, post_sted, dødsdato, email, www, info, død, omtale}
            Me.Rows.Add(rowkjendisRow)
            Return rowkjendisRow
        End Function

        Public Function FindBykjendisID(ByVal kjendisID As Integer) As kjendisRow
            Return CType(Me.Rows.Find(New Object() {kjendisID}), kjendisRow)
        End Function

        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Rows.GetEnumerator
        End Function

        Public Overrides Function Clone() As DataTable
            Dim cln As kjendisDataTable = CType(MyBase.Clone, kjendisDataTable)
            cln.InitVars()
            Return cln
        End Function

        Protected Overrides Function CreateInstance() As DataTable
            Return New kjendisDataTable
        End Function

        Friend Sub InitVars()
            Me.columnkjendisID = Me.Columns("kjendisID")
            Me.columnfornavn = Me.Columns("fornavn")
            Me.columnetternavn = Me.Columns("etternavn")
            Me.columnadresse = Me.Columns("adresse")
            Me.columntlf = Me.Columns("tlf")
            Me.columnfødselsdato = Me.Columns("fødselsdato")
            Me.columnpostnr = Me.Columns("postnr")
            Me.columnpost_sted = Me.Columns("post_sted")
            Me.columndødsdato = Me.Columns("dødsdato")
            Me.columnemail = Me.Columns("email")
            Me.columnwww = Me.Columns("www")
            Me.columninfo = Me.Columns("info")
            Me.columndød = Me.Columns("død")
            Me.columnomtale = Me.Columns("omtale")
        End Sub

        Private Sub InitClass()
            Me.columnkjendisID = New DataColumn("kjendisID", GetType(System.Int32), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnkjendisID)
            Me.columnfornavn = New DataColumn("fornavn", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnfornavn)
            Me.columnetternavn = New DataColumn("etternavn", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnetternavn)
            Me.columnadresse = New DataColumn("adresse", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnadresse)
            Me.columntlf = New DataColumn("tlf", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columntlf)
            Me.columnfødselsdato = New DataColumn("fødselsdato", GetType(System.DateTime), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnfødselsdato)
            Me.columnpostnr = New DataColumn("postnr", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnpostnr)
            Me.columnpost_sted = New DataColumn("post_sted", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnpost_sted)
            Me.columndødsdato = New DataColumn("dødsdato", GetType(System.DateTime), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columndødsdato)
            Me.columnemail = New DataColumn("email", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnemail)
            Me.columnwww = New DataColumn("www", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnwww)
            Me.columninfo = New DataColumn("info", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columninfo)
            Me.columndød = New DataColumn("død", GetType(System.Boolean), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columndød)
            Me.columnomtale = New DataColumn("omtale", GetType(System.Boolean), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnomtale)
            Me.Constraints.Add(New UniqueConstraint("Constraint1", New DataColumn() {Me.columnkjendisID}, True))
            Me.columnkjendisID.AutoIncrement = True
            Me.columnkjendisID.AllowDBNull = False
            Me.columnkjendisID.ReadOnly = True
            Me.columnkjendisID.Unique = True
            Me.columnetternavn.AllowDBNull = False
            Me.columnfødselsdato.AllowDBNull = False
            Me.columndød.AllowDBNull = False
        End Sub

        Public Function NewkjendisRow() As kjendisRow
            Return CType(Me.NewRow, kjendisRow)
        End Function

        Protected Overrides Function NewRowFromBuilder(ByVal builder As DataRowBuilder) As DataRow
            Return New kjendisRow(builder)
        End Function

        Protected Overrides Function GetRowType() As System.Type
            Return GetType(kjendisRow)
        End Function

        Protected Overrides Sub OnRowChanged(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanged(e)
            If (Not (Me.kjendisRowChangedEvent) Is Nothing) Then
                RaiseEvent kjendisRowChanged(Me, New kjendisRowChangeEvent(CType(e.Row, kjendisRow), e.Action))
            End If
        End Sub

        Protected Overrides Sub OnRowChanging(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanging(e)
            If (Not (Me.kjendisRowChangingEvent) Is Nothing) Then
                RaiseEvent kjendisRowChanging(Me, New kjendisRowChangeEvent(CType(e.Row, kjendisRow), e.Action))
            End If
        End Sub

        Protected Overrides Sub OnRowDeleted(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleted(e)
            If (Not (Me.kjendisRowDeletedEvent) Is Nothing) Then
                RaiseEvent kjendisRowDeleted(Me, New kjendisRowChangeEvent(CType(e.Row, kjendisRow), e.Action))
            End If
        End Sub

        Protected Overrides Sub OnRowDeleting(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleting(e)
            If (Not (Me.kjendisRowDeletingEvent) Is Nothing) Then
                RaiseEvent kjendisRowDeleting(Me, New kjendisRowChangeEvent(CType(e.Row, kjendisRow), e.Action))
            End If
        End Sub

        Public Sub RemovekjendisRow(ByVal row As kjendisRow)
            Me.Rows.Remove(row)
        End Sub
    End Class

    <System.Diagnostics.DebuggerStepThrough()> _
    Public Class kjendisRow
        Inherits DataRow

        Private tablekjendis As kjendisDataTable

        Friend Sub New(ByVal rb As DataRowBuilder)
            MyBase.New(rb)
            Me.tablekjendis = CType(Me.Table, kjendisDataTable)
        End Sub

        Public Property kjendisID() As Integer
            Get
                Return CType(Me(Me.tablekjendis.kjendisIDColumn), Integer)
            End Get
            Set(ByVal value As Integer)
                Me(Me.tablekjendis.kjendisIDColumn) = value
            End Set
        End Property

        Public Property fornavn() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.fornavnColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.fornavnColumn) = value
            End Set
        End Property

        Public Property etternavn() As String
            Get
                Return CType(Me(Me.tablekjendis.etternavnColumn), String)
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.etternavnColumn) = value
            End Set
        End Property

        Public Property adresse() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.adresseColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.adresseColumn) = value
            End Set
        End Property

        Public Property tlf() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.tlfColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.tlfColumn) = value
            End Set
        End Property

        Public Property fødselsdato() As Date
            Get
                Return CType(Me(Me.tablekjendis.fødselsdatoColumn), Date)
            End Get
            Set(ByVal value&)
                Me(Me.tablekjendis.fødselsdatoColumn) = value
            End Set
        End Property

        Public Property postnr() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.postnrColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.postnrColumn) = value
            End Set
        End Property

        Public Property post_sted() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.post_stedColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.post_stedColumn) = value
            End Set
        End Property

        Public Property dødsdato() As Date
            Get
                Try
                    Return CType(Me(Me.tablekjendis.dødsdatoColumn), Date)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value&)
                Me(Me.tablekjendis.dødsdatoColumn) = value
            End Set
        End Property

        Public Property email() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.emailColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.emailColumn) = value
            End Set
        End Property

        Public Property www() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.wwwColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.wwwColumn) = value
            End Set
        End Property

        Public Property info() As String
            Get
                Try
                    Return CType(Me(Me.tablekjendis.infoColumn), String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As String)
                Me(Me.tablekjendis.infoColumn) = value
            End Set
        End Property

        Public Property død() As Boolean
            Get
                Return CType(Me(Me.tablekjendis.dødColumn), Boolean)
            End Get
            Set(ByVal value&)
                Me(Me.tablekjendis.dødColumn) = value
            End Set
        End Property

        Public Property omtale() As Boolean
            Get
                Try
                    Return CType(Me(Me.tablekjendis.omtaleColumn), Boolean)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set(ByVal value As Boolean)
                Me(Me.tablekjendis.omtaleColumn) = value
            End Set
        End Property

        Public Function IsfornavnNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.fornavnColumn)
        End Function

        Public Sub SetfornavnNull()
            Me(Me.tablekjendis.fornavnColumn) = System.Convert.DBNull
        End Sub

        Public Function IsadresseNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.adresseColumn)
        End Function

        Public Sub SetadresseNull()
            Me(Me.tablekjendis.adresseColumn) = System.Convert.DBNull
        End Sub

        Public Function IstlfNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.tlfColumn)
        End Function

        Public Sub SettlfNull()
            Me(Me.tablekjendis.tlfColumn) = System.Convert.DBNull
        End Sub

        Public Function IspostnrNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.postnrColumn)
        End Function

        Public Sub SetpostnrNull()
            Me(Me.tablekjendis.postnrColumn) = System.Convert.DBNull
        End Sub

        Public Function Ispost_stedNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.post_stedColumn)
        End Function

        Public Sub Setpost_stedNull()
            Me(Me.tablekjendis.post_stedColumn) = System.Convert.DBNull
        End Sub

        Public Function IsdødsdatoNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.dødsdatoColumn)
        End Function

        Public Sub SetdødsdatoNull()
            Me(Me.tablekjendis.dødsdatoColumn) = System.Convert.DBNull
        End Sub

        Public Function IsemailNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.emailColumn)
        End Function

        Public Sub SetemailNull()
            Me(Me.tablekjendis.emailColumn) = System.Convert.DBNull
        End Sub

        Public Function IswwwNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.wwwColumn)
        End Function

        Public Sub SetwwwNull()
            Me(Me.tablekjendis.wwwColumn) = System.Convert.DBNull
        End Sub

        Public Function IsinfoNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.infoColumn)
        End Function

        Public Sub SetinfoNull()
            Me(Me.tablekjendis.infoColumn) = System.Convert.DBNull
        End Sub

        Public Function IsomtaleNull() As Boolean
            Return Me.IsNull(Me.tablekjendis.omtaleColumn)
        End Function

        Public Sub SetomtaleNull()
            Me(Me.tablekjendis.omtaleColumn) = System.Convert.DBNull
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class kjendisRowChangeEvent
        Inherits EventArgs
        
        Private eventRow As kjendisRow
        
        Private eventAction As DataRowAction
        
        Public Sub New(ByVal row As kjendisRow, ByVal action As DataRowAction)
            MyBase.New
            Me.eventRow = row
            Me.eventAction = action
        End Sub
        
        Public ReadOnly Property Row As kjendisRow
            Get
                Return Me.eventRow
            End Get
        End Property
        
        Public ReadOnly Property Action As DataRowAction
            Get
                Return Me.eventAction
            End Get
        End Property
    End Class
End Class
