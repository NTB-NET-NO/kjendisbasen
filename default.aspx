<%@ Page Language="vb" AutoEventWireup="false" Inherits="kjendisbasen._default" CodeFile="default.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>S&oslash;k i kjendisbasen</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" type="text/css" href="Styles.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="2" cellPadding="1" width="780" border="0" class="custom">
				<tr>
					<td colSpan="2" class="content">
						<H4>S&oslash;k i kjendisbasen</H4>
					</td>
					<td class="content" align="right" valign="top"><a href="edit-person.aspx">Ny person</a>
						| <a href="jubilant-list.aspx">Jubilantlister</a> | <a href="default.aspx">S&oslash;k</a></td>
				</tr>
				<tr>
					<td class="content" colSpan="3"><hr color="#990000" noShade SIZE="1">
					</td>
				</tr>
				<tr>
					<td class="content" colSpan="3">Bruk s&oslash;kefeltene til &aring; lete i kjendisbasen. 
						Personene kan redigeres, klikk p&aring; etternavnet. For andre funksjoner brukes 
						menyen oppe til h&oslash;yre.
					</td>
				</tr>
				<tr>
					<td class="content" colSpan="3"><hr color="#990000" noShade SIZE="1">
					</td>
				</tr>
				<TR>
					<TH width="150" class="header">
						F&oslash;dselsdato:</TH>
					<TD width="200" class="content"><asp:textbox id="dato" runat="server" Columns="10" Width="88px" MaxLength="10"></asp:textbox>&nbsp;(dd.mm.&aring;&aring;&aring;&aring;)</TD>
					<TD width="430" class="content"><asp:checkbox id="exactDate" runat="server" Text="Eksakt dato"></asp:checkbox>&nbsp;&nbsp;
						<asp:checkbox id="JubOnly" runat="server" Text="Kun runde jubileer"></asp:checkbox></TD>
				</TR>
				<TR>
					<TH width="150" class="header">
						Etternavn:</TH>
					<td width="200" class="content"><asp:textbox id="etternavn" runat="server" Columns="25" Width="232px"></asp:textbox></td>
					<TD width="430" class="content"><asp:radiobuttonlist id="txtSelector" runat="server" RepeatDirection="Horizontal" Height="10px" RepeatLayout="Flow"
							Width="264px">
							<asp:ListItem Value="True" Selected="True">Begynner med&amp;nbsp;&amp;nbsp;</asp:ListItem>
							<asp:ListItem Value="False">Inneholder</asp:ListItem>
						</asp:radiobuttonlist></TD>
				</TR>
				<TR>
					<TH width="150" class="header">
						Type:</TH>
					<TD width="200" class="content"><asp:dropdownlist id=TypeSelector runat="server" DataValueField="TypeID" DataTextField="Tekst" DataMember="person_type" DataSource="<%# PersonTypeDataSet %>">
							<asp:ListItem Value="-1" Selected="True">Alle</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD width="430" class="content" align="left">
						<asp:Label id="Message" runat="server" Width="350px" ForeColor="Red" Font-Bold="True"></asp:Label></TD>
				</TR>
				<tr>
					<TH width="150" class="header">
						D&oslash;d-status:</TH>
					<TD width="200" class="content"><asp:dropdownlist id="DeadSelector" runat="server">
							<asp:ListItem Value="-1" Selected="True">&lt;Alle&gt;</asp:ListItem>
							<asp:ListItem Value="0">Ikke d&#248;d</asp:ListItem>
							<asp:ListItem Value="1">D&#248;d</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD width="430" class="content" align="right"><asp:button id="srcBtn" runat="server" Text="S&oslash;k etter personer" Width="128px"></asp:button></TD>
				</tr>
				<tr>
					<td colSpan="5" align="right"><br>
						<asp:label id="Label6" runat="server" Width="596px" Font-Size="XX-Small" Font-Names="verdana">Typeforklaring: A - Opprinnelig fra Aftenpostenbasen, K - Tilh&oslash;rer kjendisbasen, P - Tilh&oslash;rer personalia </asp:label></td>
				</tr>
			</table>
			<asp:datagrid id="ResultsGrid" runat="server" Width="780px" Font-Size="X-Small" Font-Names="verdana"
				BackColor="White" BorderWidth="1px" BorderColor="#CC9966" AutoGenerateColumns="False" CellPadding="2"
				BorderStyle="None" ShowFooter="True">
				<FooterStyle Font-Bold="True" ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
				<AlternatingItemStyle ForeColor="#330099" VerticalAlign="Top" BackColor="#FFFFCC"></AlternatingItemStyle>
				<ItemStyle ForeColor="#330099" VerticalAlign="Top" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="#FFFFCC" BackColor="#990000"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="F&#248;dselsdato">
						<HeaderStyle Width="80px"></HeaderStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fodselsdato", "{0:dd.MM.yyyy}") %>'>
							</asp:Label>
						</ItemTemplate>
						<FooterTemplate>
							<asp:Label runat="server" Text='<%# String.Format("Totalt: {0}",count) %>'>
							</asp:Label>
						</FooterTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fodselsdato", "{0:dd.MM.yyyy}") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Alder">
						<HeaderStyle Width="100px"></HeaderStyle>
						<ItemStyle Font-Size="XX-Small"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# alder %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# alder %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Etternavn">
						<ItemTemplate>
							<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.etternavn") %>' NavigateUrl='<%# string.format("edit-person.aspx?kid={0}",DataBinder.Eval(Container, "DataItem.kjendisid")) %>'>
							</asp:HyperLink>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.etternavn") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="fornavn" HeaderText="Fornavn"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Informasjon">
						<ItemStyle Font-Size="XX-Small"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# adresse %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# adresse %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="D&#248;d?">
						<HeaderStyle Width="40px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# GetDod(DataBinder.Eval(Container, "DataItem.dod")) %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# GetDod(DataBinder.Eval(Container, "DataItem.dod")) %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="short" HeaderText="Type">
						<HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<FooterStyle HorizontalAlign="Center"></FooterStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
			</asp:datagrid>
			<P>
				<asp:label id="err" runat="server" Width="776px" Height="98px"></asp:label></P>
		</form>
	</body>
</HTML>
