Imports System.Data.SqlClient


Namespace kjendisbasen

Partial Class _default
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.KjendisConnection = New System.Data.SqlClient.SqlConnection
        Me.PersonTypeDataSet = New kjendisbasen.PersonTypeDataSet
        Me.PersonTypeAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        CType(Me.PersonTypeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'KjendisConnection
        '
            Me.KjendisConnection.ConnectionString = ConfigurationManager.ConnectionStrings("KjendisbasenConnectionString").ConnectionString
            ' "workstation id=""GX270-LCH"";packet size=4096;user id=kjendis;data source=MELBOURNE" & _
            ' ";persist security info=True;initial catalog=Kjendisbasen;password=kjendis"
        '
        'PersonTypeDataSet
        '
        Me.PersonTypeDataSet.DataSetName = "PersonTypeDataSet"
        Me.PersonTypeDataSet.Locale = New System.Globalization.CultureInfo("nb-NO")
        '
        'PersonTypeAdapter
        '
        Me.PersonTypeAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.PersonTypeAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "person_type", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TypeID", "TypeID"), New System.Data.Common.DataColumnMapping("Tekst", "Tekst"), New System.Data.Common.DataColumnMapping("Short", "Short")})})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT TypeID, Tekst, Short FROM dbo.person_type"
        Me.SqlSelectCommand2.Connection = Me.KjendisConnection
        CType(Me.PersonTypeDataSet, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected WithEvents KjendisConnection As System.Data.SqlClient.SqlConnection
    Protected WithEvents PersonTypeDataSet As kjendisbasen.PersonTypeDataSet
    Protected WithEvents PersonTypeAdapter As System.Data.SqlClient.SqlDataAdapter
    Protected WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        err.Text = ""
        Message.Text = ""
        Session("nextpage") = "default.aspx"

        Try
            If Not IsPostBack Then
                PersonTypeAdapter.Fill(PersonTypeDataSet)
                TypeSelector.DataBind()
                TypeSelector.Items.Insert(0, New ListItem("<Alle>", -1))

                If Session("action") = "recap" Then
                    dato.Text = Session("src_fd")
                    etternavn.Text = Session("src_en")
                    TypeSelector.SelectedValue = Session("src_type")
                    DeadSelector.SelectedValue = Session("src_dead")
                    exactDate.Checked = Session("src_exact")
                    JubOnly.Checked = Session("src_jubonly")
                    txtSelector.SelectedValue = Session("src_starts")
                    PopulateList(dato.Text, etternavn.Text, TypeSelector.SelectedValue, DeadSelector.SelectedValue, exactDate.Checked, JubOnly.Checked, txtSelector.SelectedValue)
                Else
                    TypeSelector.SelectedValue = 3
                    dato.Text = Today

                    Session("src_fd") = dato.Text
                    Session("src_en") = etternavn.Text
                    Session("src_type") = TypeSelector.SelectedValue
                    Session("src_dead") = DeadSelector.SelectedValue
                    Session("src_exact") = exactDate.Checked
                    Session("src_jubonly") = JubOnly.Checked
                    Session("src_starts") = txtSelector.SelectedValue

                    PopulateList(dato.Text, , TypeSelector.SelectedValue)
                End If
            End If
        Catch ex As Exception
            err.Text = ex.Message & "<br>" & ex.StackTrace
        End Try

        Session("action") = ""
    End Sub

    Sub PopulateList(Optional ByVal dato As String = "", Optional ByVal etternavn As String = "", _
                     Optional ByVal type As Integer = -1, Optional ByVal dod As Integer = -1, _
                     Optional ByVal exactdate As Boolean = False, Optional ByVal jubonly As Boolean = False, Optional ByVal startswith As Boolean = True)

        KjendisConnection.Open()

        Dim cmd As SqlCommand = KjendisConnection.CreateCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "FindPerson"

        'Text search option
        cmd.Parameters.Add(New SqlClient.SqlParameter("@startswith", SqlDbType.Bit))
        cmd.Parameters.Item("@startswith").Value = startswith

        'Date comparision 
        cmd.Parameters.Add(New SqlClient.SqlParameter("@exactdate", SqlDbType.Bit))
        cmd.Parameters.Item("@exactdate").Value = exactdate
        cmd.Parameters.Add(New SqlClient.SqlParameter("@jubonly", SqlDbType.Bit))
        cmd.Parameters.Item("@jubonly").Value = jubonly

        'Add the date
        If IsDate(dato) Then
            cmd.Parameters.Add(New SqlClient.SqlParameter("@fd", SqlDbType.DateTime))
            cmd.Parameters.Item("@fd").Value = dato
        End If

        'Name search
        If etternavn <> "" Then
            cmd.Parameters.Add(New SqlClient.SqlParameter("@en", SqlDbType.VarChar))
            cmd.Parameters.Item("@en").Value = etternavn
        End If

        'Type filter
        If type > -1 Then
            cmd.Parameters.Add(New SqlClient.SqlParameter("@type", SqlDbType.Int))
            cmd.Parameters.Item("@type").Value = type
        End If

        'Dead filter
        If dod > -1 Then
            cmd.Parameters.Add(New SqlClient.SqlParameter("@dod", SqlDbType.Bit))
            cmd.Parameters.Item("@dod").Value = dod
        End If

        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        count = 0

        ResultsGrid.DataSource = rdr
        ResultsGrid.DataBind()

        rdr.Close()
        KjendisConnection.Close()
    End Sub

    'Helpers
    Protected count As Integer = 0
    Protected adresse As New String("")
    Protected alder As New String("")
    Protected aft As New String("")

    Private Sub ResultsGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles ResultsGrid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim src As SqlDataReader = CType(sender, DataGrid).DataSource

            If Not src Is Nothing Then
                count += 1

                adresse = ""
                If Not IsDBNull(src("adresse")) Then
                    If src("adresse") <> "" Then
                        adresse = src("adresse") & " "
                    End If
                End If

                If Not IsDBNull(src("postnr")) Then
                    If src("postnr") <> "" Then
                        adresse &= src("postnr") & " "
                    End If
                End If

                If Not IsDBNull(src("post_sted")) Then
                    If src("post_sted") <> "" Then
                        adresse &= src("post_sted") & " "
                    End If
                End If

                If adresse <> "" Then
                    adresse = "<b>" & adresse & "</b><br>" & src("info")
                Else
                    adresse &= src("info")
                End If

                alder = src("alder") & " (" & Format(src("neste"), "dd.MM.yyy") & ")"
            End If
        End If
    End Sub

    Function GetDod(ByVal d As Boolean) As String
        If d Then
            Return "Ja"
        Else
            Return "Nei"
        End If
    End Function

    Private Sub srcBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles srcBtn.Click
        Try
            If Not IsDate(dato.Text) And etternavn.Text = "" Then
                    Message.Text = "F&oslash;dselsdato eller etternavn m&aring; fylles ut."
            Else
                Session("src_fd") = dato.Text
                Session("src_en") = etternavn.Text
                Session("src_type") = TypeSelector.SelectedValue
                Session("src_dead") = DeadSelector.SelectedValue
                Session("src_exact") = exactDate.Checked
                Session("src_jubonly") = JubOnly.Checked
                Session("src_starts") = txtSelector.SelectedValue
                PopulateList(dato.Text, etternavn.Text, TypeSelector.SelectedValue, DeadSelector.SelectedValue, exactDate.Checked, JubOnly.Checked, txtSelector.SelectedValue)
            End If
        Catch ex As Exception
            err.Text = ex.Message & "<br>" & ex.StackTrace
        End Try
    End Sub

End Class

End Namespace
