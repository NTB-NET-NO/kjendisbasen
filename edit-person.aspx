<%@ Page Language="vb" AutoEventWireup="false" Inherits="kjendisbasen.edit_person" CodeFile="edit-person.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Redigere person</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<LINK href="Styles.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript">
 � �		<!--
 � �		
 � �		function DeleteVerify(f) {
 � �			if (confirm("Er du sikker p&aring; at du vil slette " + f.Fornavn.value + " " + f.Etternavn.value + "?" ) ) {
					f.slett.value = 'true';
					f.SaveButton.click();
 � �			}
			}
			
			//-->
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table class="custom" cellSpacing="2" cellPadding="2" width="700" border="0">
				<tr>
					<td class="content">
						<H4>Redigere person i kjendisbasen</H4>
					</td>
					<td class="content" vAlign="top" align="right"><A href="edit-person.aspx">Ny person</A>
						| <A href="jubilant-list.aspx">Jubilantlister</A> | <A href="default.aspx">S&oslash;k</A></td>
				</tr>
				<tr>
					<td class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1">
					</td>
				</tr>
				<tr>
					<td class="content" colSpan="2">For &aring; oppdatere kjendisbasen m&aring; du fylle ut eller 
						gj&oslash;re endringer i skjemaet under, og klikke 'Lagre endringer'. Alle felter 
						merket med '*' m&aring; fylles ut. Du kan slette personen ved &aring; klikke 'Slett 
						person'.
					</td>
				</tr>
				<tr>
					<td class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1">
					</td>
				</tr>
			</table>
			<table class="custom" cellSpacing="2" cellPadding="2" width="700" border="0">
				<TR>
					<TH class="header" width="150">
						ID</TH>
					<TD class="content" width="550"><asp:label id="KID" runat="server" Font-Size="X-Small" Font-Names="verdana" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].kjendisID") %>' Width="136px"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:label id="Message" runat="server" Width="376px" Font-Bold="True" ForeColor="Red"></asp:label></TD>
				</TR>
				<TR>
					<TH class="header">
						*F&oslash;dselsdato</TH>
					<TD class="content"><asp:textbox id=FDato runat="server" Font-Size="X-Small" Font-Names="verdana" Text='<%# string.format("{0:dd.MM.yyyy}",DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].f�dselsdato")) %>' Width="272px" Columns="20"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;(Format: 
						dd.mm.&aring;&aring;&aring;&aring;)</TD>
				</TR>
				<TR>
					<TH class="header">
						*Etternavn</TH>
					<TD class="content"><asp:textbox id=Etternavn runat="server" Font-Size="X-Small" Font-Names="verdana" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].etternavn") %>' Width="272px" Columns="40"></asp:textbox></TD>
				</TR>
				<TR>
					<TH class="header">
						*Fornavn</TH>
					<TD class="content"><asp:textbox id=Fornavn runat="server" Font-Size="X-Small" Font-Names="verdana" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].fornavn") %>' Width="272px" Columns="40"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1" />
					</TD>
				</TR>
				<TR>
					<TH class="header">
						Adresse</TH>
					<TD class="content"><asp:textbox id=adresse runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].adresse") %>' Columns="40">
						</asp:textbox></TD>
				</TR>
				<TR>
					<TH class="header">
						Postnr/sted</TH>
					<TD class="content"><asp:textbox id=postnr runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].postnr") %>' Columns="5">
						</asp:textbox><asp:textbox id=sted runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].post_sted") %>' Width="208px" Columns="20">
						</asp:textbox></TD>
				</TR>
				<TR>
					<TH class="header">
						Telefon</TH>
					<TD class="content"><asp:textbox id=telefon runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].tlf") %>' Columns="40">
						</asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1">
					</TD>
				</TR>
				<TR>
					<TH class="header">
						Epost</TH>
					<TD class="content"><asp:textbox id=email runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].email") %>' Columns="40">
						</asp:textbox></TD>
				</TR>
				<TR>
					<TH class="header">
						Webside</TH>
					<TD class="content"><asp:textbox id="www" runat="server" Columns="40"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1">
					</TD>
				</TR>
				<TR>
					<TH class="header">
						*Type</TH>
					<TD class="content"><asp:radiobuttonlist id=TypeSelector runat="server" Font-Size="X-Small" Font-Names="verdana" Width="352px" CellSpacing="10" RepeatLayout="Flow" RepeatDirection="Horizontal" DataValueField="TypeID" DataTextField="Tekst" DataMember="person_type" DataSource="<%# PersonTypeDataSet %>" Height="5px">
						</asp:radiobuttonlist></TD>
				</TR>
				<TR>
					<TH class="header">
						Omtale ?</TH>
					<TD class="content"><asp:radiobuttonlist id=omtale_valg runat="server" Font-Size="X-Small" Font-Names="verdana" Width="352px" CellSpacing="5" RepeatLayout="Flow" RepeatDirection="Horizontal" Height="2px" SelectedValue='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].omtale") %>'>
							<asp:ListItem Value="True">Ja&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="False">Nei&#160;&#160;</asp:ListItem>
							<asp:ListItem Value="">Vet ikke</asp:ListItem>
						</asp:radiobuttonlist>&nbsp;</TD>
				</TR>
				<TR>
					<TH class="header">
						D&oslash;d ?</TH>
					<TD class="content"><asp:checkbox id=dod runat="server" Checked='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].d�d") %>'>
						</asp:checkbox>&nbsp;D&oslash;dsdato:
						<asp:textbox id=dodsdato runat="server" Text='<%# string.format("{0:dd.MM.yyyy}",DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].d�dsdato")) %>' Columns="20">
						</asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;(Format: dd.mm.&aring;&aring;&aring;&aring;)</TD>
				</TR>
				<TR>
					<TD class="content" colSpan="2">
						<hr color="#990000" noShade SIZE="1">
					</TD>
				</TR>
				<TR>
					<TH class="header" vAlign="top">
						Annet</TH>
					<TD class="content"><asp:textbox id=Info runat="server" Text='<%# DataBinder.Eval(KjendisDataSet, "Tables[kjendis].DefaultView.[0].info") %>' Width="400px" Columns="40" textMode="MultiLine" Rows="5">
						</asp:textbox></TD>
				</TR>
				<TR>
					<TD class="content" colSpan="2">
						<HR color="#990000" noShade SIZE="1">
					</TD>
				</TR>
				<TR>
					<TD class="content">
					<TD class="content" align="right"><input id="slett" type="hidden" value="false" name="slett">
						<asp:button id="SaveButton" runat="server" Text="Lagre endringer" Width="96px"></asp:button>&nbsp;&nbsp;&nbsp;
						<input id="slettBtn" onclick="DeleteVerify(this.form)" type="button" value="Slett person"
							name="slettBtn">&nbsp;&nbsp;&nbsp;
						<asp:button id="CancelBtn" runat="server" Text="Tilbake til listen" Width="128px"></asp:button></TD>
				</TR>
			</table>
			<P><asp:label id="err" runat="server" Width="696px" Height="56px"></asp:label></P>
		</form>
	</body>
</HTML>
