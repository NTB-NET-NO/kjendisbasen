Imports System.Convert
Imports System.Text
Imports System.Globalization

Namespace kjendisbasen

    Partial Class edit_person
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.KjendisConnection = New System.Data.SqlClient.SqlConnection
            Me.KjendisDataSet = New kjendisbasen.KjendisDataSet
            Me.PersonTypeAdapter = New System.Data.SqlClient.SqlDataAdapter
            Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
            Me.PersonTypeDataSet = New kjendisbasen.PersonTypeDataSet
            Me.KjendisAdapter = New System.Data.SqlClient.SqlDataAdapter
            Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
            Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
            Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
            Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
            CType(Me.KjendisDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PersonTypeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'KjendisConnection
            '
            ' Me.KjendisConnection.ConnectionString = "workstation id=""GX270-LCH"";packet size=4096;user id=kjendis;data source=MELBOURNE" & _
            ' ";persist security info=True;initial catalog=Kjendisbasen;password=kjendis"
            Me.KjendisConnection.ConnectionString = ConfigurationManager.ConnectionStrings("KjendisbasenConnectionString").ConnectionString
            '.ConnectionStrings("KjendisbasenConnectionString")
            '
            'KjendisDataSet
            '
            Me.KjendisDataSet.DataSetName = "KjendisDataSet"
            Me.KjendisDataSet.Locale = New System.Globalization.CultureInfo("nb-NO")
            '
            'PersonTypeAdapter
            '
            Me.PersonTypeAdapter.SelectCommand = Me.SqlSelectCommand2
            Me.PersonTypeAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "person_type", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TypeID", "TypeID"), New System.Data.Common.DataColumnMapping("Tekst", "Tekst"), New System.Data.Common.DataColumnMapping("Short", "Short")})})
            '
            'SqlSelectCommand2
            '
            Me.SqlSelectCommand2.CommandText = "SELECT TypeID, Tekst, Short FROM dbo.person_type"
            Me.SqlSelectCommand2.Connection = Me.KjendisConnection
            '
            'PersonTypeDataSet
            '
            Me.PersonTypeDataSet.DataSetName = "PersonTypeDataSet"
            Me.PersonTypeDataSet.Locale = New System.Globalization.CultureInfo("nb-NO")
            '
            'KjendisAdapter
            '
            Me.KjendisAdapter.DeleteCommand = Me.SqlDeleteCommand1
            Me.KjendisAdapter.InsertCommand = Me.SqlInsertCommand1
            Me.KjendisAdapter.SelectCommand = Me.SqlSelectCommand1
            Me.KjendisAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "kjendis", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("kjendisID", "kjendisID"), New System.Data.Common.DataColumnMapping("fornavn", "fornavn"), New System.Data.Common.DataColumnMapping("etternavn", "etternavn"), New System.Data.Common.DataColumnMapping("adresse", "adresse"), New System.Data.Common.DataColumnMapping("tlf", "tlf"), New System.Data.Common.DataColumnMapping("f�dselsdato", "f�dselsdato"), New System.Data.Common.DataColumnMapping("postnr", "postnr"), New System.Data.Common.DataColumnMapping("post_sted", "post_sted"), New System.Data.Common.DataColumnMapping("d�dsdato", "d�dsdato"), New System.Data.Common.DataColumnMapping("email", "email"), New System.Data.Common.DataColumnMapping("www", "www"), New System.Data.Common.DataColumnMapping("info", "info"), New System.Data.Common.DataColumnMapping("d�d", "d�d"), New System.Data.Common.DataColumnMapping("type", "type"), New System.Data.Common.DataColumnMapping("omtale", "omtale")})})
            Me.KjendisAdapter.UpdateCommand = Me.SqlUpdateCommand1
            '
            'SqlDeleteCommand1
            '
            Me.SqlDeleteCommand1.CommandText = "DELETE FROM dbo.kjendis WHERE (kjendisID = @Original_kjendisID) AND (adresse = @O" & _
            "riginal_adresse OR @Original_adresse IS NULL AND adresse IS NULL) AND (d�d = @Or" & _
            "iginal_d�d) AND (d�dsdato = @Original_d�dsdato OR @Original_d�dsdato IS NULL AND" & _
            " d�dsdato IS NULL) AND (email = @Original_email OR @Original_email IS NULL AND e" & _
            "mail IS NULL) AND (etternavn = @Original_etternavn) AND (fornavn = @Original_for" & _
            "navn OR @Original_fornavn IS NULL AND fornavn IS NULL) AND (f�dselsdato = @Origi" & _
            "nal_f�dselsdato) AND (omtale = @Original_omtale OR @Original_omtale IS NULL AND " & _
            "omtale IS NULL) AND (post_sted = @Original_post_sted OR @Original_post_sted IS N" & _
            "ULL AND post_sted IS NULL) AND (postnr = @Original_postnr OR @Original_postnr IS" & _
            " NULL AND postnr IS NULL) AND (tlf = @Original_tlf OR @Original_tlf IS NULL AND " & _
            "tlf IS NULL) AND (type = @Original_type) AND (www = @Original_www OR @Original_w" & _
            "ww IS NULL AND www IS NULL)"
            Me.SqlDeleteCommand1.Connection = Me.KjendisConnection
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_kjendisID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "kjendisID", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_adresse", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "adresse", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_d�d", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "d�d", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_d�dsdato", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "d�dsdato", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_email", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "email", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_etternavn", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "etternavn", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_fornavn", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fornavn", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_f�dselsdato", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "f�dselsdato", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_omtale", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "omtale", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_post_sted", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "post_sted", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_postnr", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "postnr", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_tlf", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tlf", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_type", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "type", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_www", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "www", System.Data.DataRowVersion.Original, Nothing))
            '
            'SqlInsertCommand1
            '
            Me.SqlInsertCommand1.CommandText = "INSERT INTO dbo.kjendis(fornavn, etternavn, adresse, tlf, f�dselsdato, postnr, po" & _
            "st_sted, d�dsdato, email, www, info, d�d, type, omtale) VALUES (@fornavn, @etter" & _
            "navn, @adresse, @tlf, @f�dselsdato, @postnr, @post_sted, @d�dsdato, @email, @www" & _
            ", @info, @d�d, @type, @omtale); SELECT kjendisID, fornavn, etternavn, adresse, t" & _
            "lf, f�dselsdato, postnr, post_sted, d�dsdato, email, www, info, d�d, type, omtal" & _
            "e FROM dbo.kjendis WHERE (kjendisID = @@IDENTITY)"
            Me.SqlInsertCommand1.Connection = Me.KjendisConnection
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fornavn", System.Data.SqlDbType.VarChar, 255, "fornavn"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@etternavn", System.Data.SqlDbType.VarChar, 255, "etternavn"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@adresse", System.Data.SqlDbType.VarChar, 255, "adresse"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@tlf", System.Data.SqlDbType.VarChar, 15, "tlf"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@f�dselsdato", System.Data.SqlDbType.DateTime, 8, "f�dselsdato"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@postnr", System.Data.SqlDbType.VarChar, 5, "postnr"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@post_sted", System.Data.SqlDbType.VarChar, 255, "post_sted"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@d�dsdato", System.Data.SqlDbType.DateTime, 8, "d�dsdato"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 255, "email"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@www", System.Data.SqlDbType.VarChar, 255, "www"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@info", System.Data.SqlDbType.VarChar, 2147483647, "info"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@d�d", System.Data.SqlDbType.Bit, 1, "d�d"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@type", System.Data.SqlDbType.Int, 4, "type"))
            Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@omtale", System.Data.SqlDbType.Bit, 1, "omtale"))
            '
            'SqlSelectCommand1
            '
            Me.SqlSelectCommand1.CommandText = "SELECT kjendisID, fornavn, etternavn, adresse, tlf, f�dselsdato, postnr, post_ste" & _
            "d, d�dsdato, email, www, info, d�d, type, omtale FROM dbo.kjendis WHERE (kjendis" & _
            "ID = @kid)"
            Me.SqlSelectCommand1.Connection = Me.KjendisConnection
            Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@kid", System.Data.SqlDbType.Int, 4, "kjendisID"))
            '
            'SqlUpdateCommand1
            '
            ' "AND (d�d = @Original_d�d) AND (d�dsdato = @Original_d�dsdato OR @Original_d�dsdato IS NULL AND d�dsdato IS NULL) 
            ' " AND (f�dselsdato = @Original_f�dselsdato OR @Original_f�dselsdato IS NULL) " & _
            Me.SqlUpdateCommand1.CommandText = "UPDATE dbo.kjendis SET fornavn = @fornavn, etternavn = @etternavn, adresse = @adresse, tlf = @tlf, f�dselsdato = @f�dselsdato, postnr = @postnr, post_sted = @post_sted, d�dsdato = @d�dsdato, email = @email, www = @www, info = @info, d�d = @d�d, type = @type, omtale = @omtale " & _
                " WHERE (kjendisID = @Original_kjendisID) AND (adresse = @Original_adresse OR @Original_adresse IS NULL AND adresse IS NULL) " & _
                " AND (email = @Original_email OR @Original_email IS NULL AND email IS NULL) " & _
                " AND (etternavn = @Original_etternavn) " & _
                " AND (fornavn = @Original_fornavn OR @Original_fornavn IS NULL AND fornavn IS NULL) " & _
                " AND (omtale = @Original_omtale OR @Original_omtale IS NULL AND omtale IS NULL) " & _
                " AND (post_sted = @Original_post_sted OR @Original_post_sted IS NULL AND post_sted IS NULL) " & _
                " AND (postnr = @Original_postnr OR @Original_postnr IS NULL AND postnr IS NULL) " & _
                " AND (tlf = @Original_tlf OR @Original_tlf IS NULL AND tlf IS NULL) " & _
                " AND (type = @Original_type) AND (www = @Original_www OR @Original_www IS NULL AND www IS NULL); " & _
                " SELECT kjendisID, fornavn, etternavn, adresse, tlf, f�dselsdato, postnr, post_sted, d�dsdato, email, www, info, d�d, type, omtale " & _
                " FROM dbo.kjendis " & _
                " WHERE (kjendisID = @kjendisID)"
            Me.SqlUpdateCommand1.Connection = Me.KjendisConnection
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@fornavn", System.Data.SqlDbType.VarChar, 255, "fornavn"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@etternavn", System.Data.SqlDbType.VarChar, 255, "etternavn"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@adresse", System.Data.SqlDbType.VarChar, 255, "adresse"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@tlf", System.Data.SqlDbType.VarChar, 15, "tlf"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@f�dselsdato", System.Data.SqlDbType.DateTime, 8, "f�dselsdato"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@postnr", System.Data.SqlDbType.VarChar, 5, "postnr"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@post_sted", System.Data.SqlDbType.VarChar, 255, "post_sted"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@d�dsdato", System.Data.SqlDbType.DateTime, 8, "d�dsdato"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.VarChar, 255, "email"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@www", System.Data.SqlDbType.VarChar, 255, "www"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@info", System.Data.SqlDbType.VarChar, 2147483647, "info"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@d�d", System.Data.SqlDbType.Bit, 1, "d�d"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@type", System.Data.SqlDbType.Int, 4, "type"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@omtale", System.Data.SqlDbType.Bit, 1, "omtale"))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_kjendisID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "kjendisID", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_adresse", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "adresse", System.Data.DataRowVersion.Original, Nothing))
            'Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_d�d", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "dod", System.Data.DataRowVersion.Original, Nothing))
            'Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_d�dsdato", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "dodsdato", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_email", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "email", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_etternavn", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "etternavn", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_fornavn", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fornavn", System.Data.DataRowVersion.Original, Nothing))
            ' Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_f�dselsdato", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fodselsdato", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_omtale", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "omtale", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_post_sted", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "post_sted", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_postnr", System.Data.SqlDbType.VarChar, 5, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "postnr", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_tlf", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tlf", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_type", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "type", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_www", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "www", System.Data.DataRowVersion.Original, Nothing))
            Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@kjendisID", System.Data.SqlDbType.Int, 4, "kjendisID"))
            CType(Me.KjendisDataSet, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PersonTypeDataSet, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Protected WithEvents KjendisConnection As System.Data.SqlClient.SqlConnection
        Protected WithEvents KjendisDataSet As kjendisbasen.KjendisDataSet
        Protected WithEvents PersonTypeAdapter As System.Data.SqlClient.SqlDataAdapter
        Protected WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
        Protected WithEvents PersonTypeDataSet As kjendisbasen.PersonTypeDataSet
        Protected WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
        Protected WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
        Protected WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
        Protected WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
        Protected WithEvents KjendisAdapter As System.Data.SqlClient.SqlDataAdapter


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Public t As Integer

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Try

                If Session("nextpage") = "" Then Session("nextpage") = "default.aspx"
                PersonTypeAdapter.Fill(PersonTypeDataSet)

                If Page.IsPostBack Then
                    If IsNumeric(KID.Text) Then
                        Dim intTmp As Integer = Convert.ToInt32(KID.Text)
                        KjendisAdapter.SelectCommand.Parameters("@kid").Value = intTmp
                        KjendisAdapter.Fill(KjendisDataSet)
                    End If
                ElseIf IsNumeric(Page.Request("kid")) Then
                    Response.Charset = "utf-8"
                    Dim intTmp As Integer = Page.Request("kid")


                    KjendisAdapter.SelectCommand.Parameters("@kid").Value = Convert.ToInt32(intTmp) ' Page.Request("kid")


                    KjendisAdapter.Fill(KjendisDataSet)
                    Page.DataBind()
                Else
                    TypeSelector.DataBind()
                    TypeSelector.SelectedValue = 4
                    KID.Text = "Ny person"
                End If

                If Request.Form("slett") = "true" Then slettBtn_Click()


            Catch ex As Exception
                Response.Write(ex.Message.ToString())
                Response.Write(ex.StackTrace.ToString())

            End Try

        End Sub

        Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click

            If Fornavn.Text = "" Or Etternavn.Text = "" Or Not IsDate(FDato.Text) Then
                Message.Text = "F�dselsdato, etternavn og fornavn m&aring; fylles ut."
            Else

                If KID.Text.StartsWith("Ny") Then
                    Dim r As DataRow = KjendisDataSet.Tables(0).NewRow()
                    r("f�dselsdato") = FDato.Text
                    r("fornavn") = Fornavn.Text
                    r("etternavn") = Etternavn.Text
                    r("adresse") = adresse.Text
                    r("postnr") = postnr.Text
                    r("post_sted") = sted.Text
                    r("tlf") = telefon.Text
                    r("www") = www.Text
                    r("email") = email.Text
                    r("d�d") = dod.Checked
                    If IsDate(dodsdato.Text) Then r("d�dsdato") = dodsdato.Text
                    r("info") = Info.Text
                    r("type") = TypeSelector.SelectedValue

                    Select Case omtale_valg.SelectedValue
                        Case "True"
                            r("omtale") = True
                        Case "False"
                            r("omtale") = False
                    End Select

                    'Skip back to the new record
                    Session("src_fd") = FDato.Text
                    Session("src_exact") = True
                    Session("src_en") = ""
                    Session("src_type") = -1
                    Session("src_dead") = -1
                    Session("src_jubonly") = False
                    Session("src_starts") = True

                    Try
                        KjendisDataSet.Tables("kjendis").Rows.Add(r)
                    Catch ex As Exception
                        Message.Text = "Lagring feilet. Sjekk felter og fors�k igjen."
                        err.Text = ex.Message & "<br>" & ex.StackTrace
                    End Try
                Else
                    ' If (KjendisDataSet.Tables("kjendis").Rows.Count > 0) Then
                    ' We must get the person with this kid.text
                    Dim personId As Int32 = Convert.ToInt32(KID.Text)

                    ' Get the row by PersonId
                    KjendisDataSet.Tables("kjendis").Rows(0)("f�dselsdato") = FDato.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("etternavn") = Etternavn.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("fornavn") = Fornavn.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("adresse") = adresse.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("postnr") = postnr.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("post_sted") = sted.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("tlf") = telefon.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("www") = www.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("email") = email.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("d�d") = dod.Checked

                    ' Dim deathDate = Convert.ToDateTime(dodsdato.Text)
                    Dim d As Date
                    If Date.TryParseExact(dodsdato.Text, "dd.MM.yyyy", Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.None, d) Then
                        KjendisDataSet.Tables("kjendis").Rows(0)("d�dsdato") = dodsdato.Text
                    End If
                    ' KjendisDataSet.Tables("kjendis").Rows(0)("Original_d�d") = dodsdato.Text
                    'If Date.TryParseExact(dodsdato.Text, "dd.MM.yyyy", Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.None, d) Then
                    '    KjendisDataSet.Tables("kjendis").Rows(0)("Original_d�d") = dodsdato.Text
                    'End If

                    KjendisDataSet.Tables("kjendis").Rows(0)("info") = Info.Text
                    KjendisDataSet.Tables("kjendis").Rows(0)("type") = TypeSelector.SelectedValue

                    Select Case omtale_valg.SelectedValue
                        Case "True"
                            KjendisDataSet.Tables("kjendis").Rows(0)("omtale") = True
                        Case "False"
                            KjendisDataSet.Tables("kjendis").Rows(0)("omtale") = False
                        Case Else
                            KjendisDataSet.Tables("kjendis").Rows(0)("omtale") = DBNull.Value
                    End Select
                    ' End If

                End If

                Try
                    KjendisAdapter.Update(KjendisDataSet)
                    Session("action") = "recap"
                    Response.Redirect(Session("nextpage"), True)
                Catch ex As Exception
                    Message.Text = "Lagring feilet. Sjekk felter og fors�k igjen."
                    err.Text = ex.Message & "<br>" & ex.StackTrace
                End Try
            End If
        End Sub

        Function GetPersonType(ByVal i As Object) As String
            If Not i Is Nothing Then
                Return i
            Else
                Return 4
            End If
        End Function

        'Hack to get the typeselector to show the correct selection
        Private Sub omtale_valg_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles omtale_valg.DataBinding
            TypeSelector.SelectedValue = KjendisDataSet.Tables("kjendis").Rows(0)("type")
        End Sub

        Private Sub slettBtn_Click()
            If IsNumeric(KID.Text) Then
                Try
                    KjendisDataSet.Tables("kjendis").Rows(0).Delete()
                    KjendisAdapter.Update(KjendisDataSet)
                    Session("action") = "recap"
                    Response.Redirect(Session("nextpage"), True)
                Catch ex As Exception
                    Message.Text = "Sletting feilet."
                    err.Text = ex.Message & "<br>" & ex.StackTrace
                End Try
            End If
        End Sub

        Private Sub CancelBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelBtn.Click
            Session("action") = "recap"
            Response.Redirect(Session("nextpage"), True)
        End Sub

    End Class

End Namespace
