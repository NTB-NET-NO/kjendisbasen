<%@ Page Language="vb" AutoEventWireup="false" Inherits="kjendisbasen.JubilantListForm" CodeFile="jubilant-list.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Generere jubilantlister</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" type="text/css" href="Styles.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" width="800" border="0" class="custom">
				<tr>
					<td class="content">
						<H4>Generere jubilantlister</H4>
					</td>
					<td class="content" align="right" valign="top"><a href="edit-person.aspx">Ny 
							person</a> | <a href="jubilant-list.aspx">Jubilantlister</a> | <a href="default.aspx">
							S&oslash;k</a></td>
				</tr>
				<tr>
					<td class="content" colSpan="2"><hr color="#990000" noShade SIZE="1">
					</td>
				</tr>
			</table>
			<table cellSpacing="0" width="800" border="0" class="custom">
				<tr>
					<td><asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="verdana" Font-Size="XX-Small">Velg startdato:</asp:label><asp:calendar id="from_date" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="176px"
							ShowGridLines="True" Height="148px" BorderColor="#FFCC66" BorderWidth="1px" BackColor="#FFFFCC" ForeColor="#663399" DayNameFormat="FirstTwoLetters">
							<TodayDayStyle ForeColor="White" BackColor="#FFCC66"></TodayDayStyle>
							<SelectorStyle BackColor="#FFCC66"></SelectorStyle>
							<NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC"></NextPrevStyle>
							<DayHeaderStyle Height="1px" BackColor="#FFCC66"></DayHeaderStyle>
							<SelectedDayStyle Font-Bold="True" BackColor="#CCCCFF"></SelectedDayStyle>
							<TitleStyle Font-Size="9pt" Font-Bold="True" ForeColor="#FFFFCC" BackColor="#990000"></TitleStyle>
							<OtherMonthDayStyle ForeColor="#CC9966"></OtherMonthDayStyle>
						</asp:calendar></td>
					<td vAlign="top" align="center">
						<P><asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
								ForeColor="DarkBlue">Her kan du generere sjekklister for jubilanttjenesten.</asp:label></P>
						<P><asp:label id="Label4" runat="server" Font-Names="verdana" Font-Size="X-Small" Width="331px"
								Height="80px">Alle registrerte jubilanter med runde jubileer innenfor valgte datoer vil vises. Velg f&oslash;rst start-dato til venstre. Listen vil n&aring; vise jubilanter for denne datoen. Om du &oslash;nsker jubilanter for et tidsinterval, velg ogs&aring; slutt-dato til h&oslash;yre.</asp:label></P>
						<P><asp:label id="Label5" runat="server" Font-Names="verdana" Font-Size="X-Small" Width="331px"
								Height="8px" Font-Italic="True">Jubilantene kan redigeres, klikk p&aring; etternavnet.</asp:label></P>
					</td>
					<td align="right"><asp:label id="Label2" runat="server" Font-Bold="True" Font-Names="verdana" Font-Size="XX-Small">Velg sluttdato:</asp:label><asp:calendar id="to_date" runat="server" Font-Names="Verdana" Font-Size="8pt" Width="175px" ShowGridLines="True"
							Height="148" BorderColor="#FFCC66" BorderWidth="1px" BackColor="#FFFFCC" ForeColor="#663399" DayNameFormat="FirstTwoLetters">
							<TodayDayStyle ForeColor="White" BackColor="#FFCC66"></TodayDayStyle>
							<SelectorStyle BackColor="#FFCC66"></SelectorStyle>
							<NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC"></NextPrevStyle>
							<DayHeaderStyle Height="1px" BackColor="#FFCC66"></DayHeaderStyle>
							<SelectedDayStyle Font-Bold="True" BackColor="#CCCCFF"></SelectedDayStyle>
							<TitleStyle Font-Size="9pt" Font-Bold="True" ForeColor="#FFFFCC" BackColor="#990000"></TitleStyle>
							<OtherMonthDayStyle ForeColor="#CC9966"></OtherMonthDayStyle>
						</asp:calendar></td>
				</tr>
				<tr>
					<td colspan="3" align="right"><br>
						<asp:Label id="Label6" runat="server" Font-Size="XX-Small" Font-Names="verdana" Width="602px">Typeforklaring: A - Opprinnelig fra Aftenpostenbasen, K - Tilh&oslash;rer kjendisbasen, P - Tilh&oslash;rer personalia </asp:Label></td>
				</tr>
			</table>
			<asp:datagrid id="ResultsGrid" runat="server" Font-Names="verdana" Font-Size="X-Small" Width="800px"
				BorderColor="#CC9966" BorderWidth="1px" BackColor="White" ShowFooter="True" BorderStyle="None"
				CellPadding="2" AutoGenerateColumns="False">
				<FooterStyle Font-Bold="True" ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
				<AlternatingItemStyle ForeColor="#330099" VerticalAlign="Top" BackColor="#FFFFCC"></AlternatingItemStyle>
				<ItemStyle ForeColor="#330099" VerticalAlign="Top" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="#FFFFCC" BackColor="#990000"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="F&#248;dselsdato">
						<HeaderStyle Width="80px"></HeaderStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fodselsdato", "{0:dd.MM.yyyy}") %>'>
							</asp:Label>
						</ItemTemplate>
						<FooterTemplate>
							<asp:Label runat="server" Text='<%# String.Format("Totalt: {0}",count) %>'>
							</asp:Label>
						</FooterTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fodselsdato", "{0:dd.MM.yyyy}") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Alder">
						<HeaderStyle Width="100px"></HeaderStyle>
						<ItemStyle Font-Size="XX-Small"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# alder %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# alder %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Etternavn">
						<ItemTemplate>
							<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.etternavn") %>' NavigateUrl='<%# string.format("edit-person.aspx?kid={0}",DataBinder.Eval(Container, "DataItem.kjendisid")) %>'>
							</asp:HyperLink>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.etternavn") %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="fornavn" HeaderText="Fornavn">
						<HeaderStyle Width="150px"></HeaderStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Informasjon">
						<ItemStyle Font-Size="XX-Small"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# adresse %>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# adresse %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Omt">
						<HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# omtale %>'>
							</asp:Label>
						</ItemTemplate>
						<FooterStyle HorizontalAlign="Center"></FooterStyle>
						<EditItemTemplate>
							<asp:TextBox runat="server" Text='<%# omtale %>'>
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="short" HeaderText="Type">
						<HeaderStyle HorizontalAlign="Center" Width="40px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<FooterStyle HorizontalAlign="Center"></FooterStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn HeaderText="D&#248;d">
						<HeaderStyle Width="20px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<input type="checkbox" name="dod" value='<%# kid %>'>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
			</asp:datagrid>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="800" border="0">
				<TR>
					<TD width="400">
						<asp:Label id="Notice" runat="server" Width="312px" Font-Size="XX-Small" Font-Names="verdana"
							Font-Bold="True" ForeColor="Red"></asp:Label></TD>
					<TD width="400" align="right">
						<asp:Button id="Update" runat="server" Width="214px" Text="Oppdater d&oslash;demarkeringer"></asp:Button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
