Namespace kjendisbasen

Partial Class JubilantListForm
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.KjendisConnection = New System.Data.SqlClient.SqlConnection
        '
        'KjendisConnection
        '
        Me.KjendisConnection.ConnectionString = "workstation id=""GX270-LCH"";packet size=4096;user id=kjendis;data source=MELBOURNE" & _
        ";persist security info=True;initial catalog=Kjendisbasen;password=kjendis"

    End Sub
    Protected WithEvents KjendisConnection As System.Data.SqlClient.SqlConnection


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Session("nextpage") = "jubilant-list.aspx"
        Notice.Text = ""

        If Not IsPostBack() Then
            If Session("action") = "recap" And Not Session("from_date") Is Nothing Then
                from_date.SelectedDate = Session("from_date")
                to_date.SelectedDate = Session("to_date")
                to_date.VisibleDate = to_date.SelectedDate
                from_date.VisibleDate = from_date.SelectedDate
            Else
                from_date.SelectedDate = Today
                Session("from_date") = Today
                to_date.SelectedDate = Nothing
                Session("to_date") = Nothing
            End If
            Session("action") = ""
            PopulateList()
        End If
    End Sub

    Private Sub from_date_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles from_date.SelectionChanged
        from_date.VisibleDate = from_date.SelectedDate
        Session("from_date") = from_date.SelectedDate

        If from_date.SelectedDate > to_date.SelectedDate Then to_date.SelectedDate = Nothing
        If from_date.SelectedDate.Year < to_date.SelectedDate.Year Then
            to_date.SelectedDate = from_date.SelectedDate.Year & "-12-31"
            Session("to_date") = from_date.SelectedDate.Year & "-12-31"
            to_date.VisibleDate = from_date.SelectedDate.Year & "-12-31"
        End If
        to_date.VisibleDate = from_date.SelectedDate

        PopulateList()
    End Sub

    Private Sub to_date_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles to_date.SelectionChanged
        to_date.VisibleDate = to_date.SelectedDate
        Session("to_date") = to_date.SelectedDate

        If from_date.SelectedDate > to_date.SelectedDate Then from_date.SelectedDate = to_date.SelectedDate
        If from_date.SelectedDate.Year < to_date.SelectedDate.Year Then
            from_date.SelectedDate = to_date.SelectedDate.Year & "-01-01"
            Session("from_date") = to_date.SelectedDate.Year & "-01-01"
            from_date.VisibleDate = to_date.SelectedDate.Year & "-01-01"
        End If
        from_date.VisibleDate = from_date.SelectedDate

        PopulateList()
    End Sub

    Sub PopulateList()
        KjendisConnection.Open()

        Dim cmd As SqlClient.SqlCommand = KjendisConnection.CreateCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "GetJubilantLister"

        cmd.Parameters.Add(New SqlClient.SqlParameter("@dato", Data.SqlDbType.DateTime))
        cmd.Parameters.Item("@dato").Value = from_date.SelectedDate

        If to_date.SelectedDate <> Date.MinValue Then
            cmd.Parameters.Add(New SqlClient.SqlParameter("@todato", Data.SqlDbType.DateTime))
            cmd.Parameters.Item("@todato").Value = to_date.SelectedDate
        End If

        Dim rdr As SqlClient.SqlDataReader = cmd.ExecuteReader()
        count = 0

        ResultsGrid.DataSource = rdr
        ResultsGrid.DataBind()

        rdr.Close()
        KjendisConnection.Close()
    End Sub

    'Helpers
    Protected count As Integer
    Protected kid As Integer
    Protected adresse As New String("")
    Protected alder As New String("")
    Protected omtale As New String("")
    Protected aft As New String("")

    Private Sub ResultsGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles ResultsGrid.ItemCreated

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim src As SqlClient.SqlDataReader = CType(sender, DataGrid).DataSource

            If Not src Is Nothing Then
                count += 1

                kid = src("KjendisID")

                adresse = ""
                If Not IsDBNull(src("adresse")) Then
                    If src("adresse") <> "" Then
                        adresse = src("adresse") & " "
                    End If
                End If

                If Not IsDBNull(src("postnr")) Then
                    If src("postnr") <> "" Then
                        adresse &= src("postnr") & " "
                    End If
                End If

                If Not IsDBNull(src("post_sted")) Then
                    If src("post_sted") <> "" Then
                        adresse &= src("post_sted") & " "
                    End If
                End If

                If adresse <> "" Then
                    adresse = "<b>" & adresse & "</b><br>" & src("info")
                Else
                    adresse &= src("info")
                End If

                alder = src("alder") & " (" & Format(src("neste"), "dd.MM.yyy") & ")"

                If IsDBNull(src("omtale")) Then
                    omtale = "?"
                ElseIf src("omtale") Then
                    omtale = "Ja"
                Else
                    omtale = "Nei"
                End If
            End If
        End If
    End Sub

    Private Sub Update_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Update.Click
        If Request.Form("dod") <> "" Then

            Dim query As String = "('" & Request.Form("dod") & "')"
            query = query.Replace(",", "','")
                query = "UPDATE kjendis SET d�d = 1 where kjendisid IN " & query

                KjendisConnection.Open()
                Dim cmd As SqlClient.SqlCommand = KjendisConnection.CreateCommand()
                cmd.CommandType = CommandType.Text
                cmd.CommandText = query
                Notice.Text = cmd.ExecuteNonQuery() & " personer oppdatert som d�de."
            KjendisConnection.Close()
            PopulateList()
        End If
    End Sub
End Class

End Namespace
