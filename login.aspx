<%@ Page Language="vb" AutoEventWireup="false" Inherits="kjendisbasen.login" CodeFile="login.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Logg inn</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" type="text/css" href="Styles.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<h4>Logg inn i Kjendisbasen</h4>
			<P>For &aring; s&oslash;ke etter personer eller generere<BR>
				jubilantlister m&aring; du logge inn.</P>
			<asp:label cssclass="alert" id="Message" runat="server"></asp:label>
			<TABLE id="Table1" cellSpacing="2" cellPadding="2" width="300" border="0" class="custom">
				<TR>
					<TH width="100" class=header>
						Brukernavn:</TH>
					<TD width="200" class=content><asp:textbox id="Un" runat="server" Columns="18" Width="152px"></asp:textbox></TD>
				</TR>
				<TR>
					<TH width="100" class=header>
						Passord:</TH>
					<TD width="200" class=content><asp:textbox id="Pwd" runat="server" Columns="18" TextMode="Password" Width="152px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<p></p>
			<asp:button id="LoginBtn" runat="server" Width="240px" Text="Logg inn i kjendisbasen"></asp:button>
			<P></P>
		</form>
	</body>
</HTML>
